var _ = require('lodash');
var events = require('events');
var util = require('util');

function Plugin(name, connector) {

	if (_.isUndefined(name) || _.isNull(name) || _.isEmpty(name)) {
		throw new Error('argument \'name\' invalid');
	}

	if (_.isUndefined(connector) || _.isNull(connector)) {
		throw new Error('argument \'connector\' invalid');
	}

	events.EventEmitter.call(this);

	this.connector = connector;
	this.name = name;

	this.run = function () {
		connector.open();

		self.emit('run');
	};

	this.convert = function (data) {
		throw new Error('not implemented');
	};

	var self = this;
	connector.on('data', function (data) {

		data = self.convert(data);

		self.emit('data', data);
	});
};

util.inherits(Plugin, events.EventEmitter);

module.exports = Plugin;