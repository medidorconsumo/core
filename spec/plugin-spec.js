var core = require('../index');
var Plugin = core.Plugin;
var Connector = core.Connector;

describe('O Plugin', function() {

	var plugin,
		connectorMock;

	beforeEach(function () {
		connectorMock = new Connector();

		spyOn(connectorMock, 'open').andCallFake(function () {
			connectorMock.state = 'opened';
			connectorMock.emit('data', 4);
		});

		plugin = new Plugin('voltimetro', connectorMock);
	});

	describe('Quando inicializar', function () {

		it('deve receber um connector', function () {
			expect(plugin.connector).toBe(connectorMock);
			expect(plugin.name).toBe('voltimetro');
		});
	});

	describe('Quando chamar o metodo \'run\'', function () {

		it('deve lancar uma excecao se o metodo \'convert\' nao estiver implementado', function () {

			expect(function () {
				plugin.run();
			}).toThrow();
		});

		it('deve executar um evento', function () {
			var spy = jasmine.createSpy();

			plugin.convert = jasmine.createSpy()
			plugin.on('run', spy);

			plugin.run();

			expect(spy).toHaveBeenCalled();
		});

		it('deve chamar o metodo \'open\' do connector', function () {

			expect(plugin.connector.state).toBe('closed');

			plugin.convert = jasmine.createSpy()
			plugin.run();

			expect(plugin.connector.state).toBe('opened');
		});

		it('deve observar os eventos de recebimento de dados do conector', function (done) {

			plugin.convert = function (data) {

				expect(data).toBe(4);

				done();
			};

			plugin.run();
		});
	});
});