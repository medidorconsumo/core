var events = require('events');
var util = require('util');

var Connector = function () {

    events.EventEmitter.call(this);

    this.state = 'closed';

    this.open = function () {
        throw new Error('not implemented');
    }
};

util.inherits(Connector, events.EventEmitter);

module.exports = Connector;