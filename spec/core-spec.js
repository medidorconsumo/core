var Plugin = require('../lib/plugin');
var Connector = require('../lib/connector');
var core = require('../index');

describe('Core', function () {
	it('A propriedade \'Plugin\' deve retornar uma classe Plugin', function () {
		expect(core.Plugin).toBe(Plugin);
	});

	it('A propriedade \'Connector\' deve retornar uma classe Connector', function () {
		expect(core.Connector).toBe(Connector);
	});
});