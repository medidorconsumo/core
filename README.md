Modulo Boilerplate
==================

Contém as classes fundamentais para o funcionamento do software

Estrutura de arquivos
---------------------
* lib
* spec
* index.js
* .gitignore
* .npmignore
* package.json
* README.md

Instalando dependências
-----------------------

npm install

Rodando todos os testes
-----------------------

npm test